Source: ripser
Section: math
Priority: optional
Maintainer: Gard Spreemann <gspr@nonempty.org>
Build-Depends:
 debhelper-compat (= 13),
 scdoc
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: http://ripser.org
Vcs-Browser: https://salsa.debian.org/gspr/ripser
Vcs-Git: https://salsa.debian.org/gspr/ripser.git -b debian/sid

Package: ripser
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Fast computation of persistent homology of flag complexes
 Ripser computes persistent homology of flag complexes (such as
 Vietoris-Rips complexes) only, allowing significant gains in
 computation time and memory usage over the general situation.
 .
 See https://arxiv.org/abs/1908.02518.
